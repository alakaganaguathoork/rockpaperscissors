package com.company;

import java.util.Random;
import java.util.Scanner;

public class Game {
    final String rock = "rock";
    final String paper = "paper";
    final String scissors = "scissors";
    String botMove;
    String playerMove;

    public void playGame() {
        playerStroke();
        botStroke();
        results(playerMove, botMove);
    }

    public void playerStroke() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Make a move! (rock/paper/scissors)");

        playerMove = scanner.nextLine().toLowerCase();

        if (!playerMove.equals(rock) && !playerMove.equals(paper) && !playerMove.equals(scissors)) {
            System.out.println("Wrong input!");
            playerStroke();
        }
    }
    public void botStroke() {
        Random random = new Random();
        int botNumber = random.nextInt(3);

        switch (botNumber) {
            case 0:
                botMove = rock;
                break;
            case 1:
                botMove = paper;
                break;
            case 2:
                botMove = scissors;
                break;
        }

        System.out.println("Computer chose " + botMove + "!");
    }

    public boolean playerWins(String playerMove, String botMove) {
        if (playerMove.equals("rock")) {
            return botMove.equals("scissors");
        } else if (playerMove.equals(("paper"))) {
            return botMove.equals("rock");
        } else {
            return botMove.equals("paper");
        }
    }

    public void results(String playerMove, String botMove) {
        if (playerMove.equals(botMove)) {
            System.out.println("It's a draw!");
        } else if (playerWins(playerMove, botMove)) {
            System.out.println("Player wins!");
        } else {
            System.out.println("Computer wins!");
        }
    }
}
